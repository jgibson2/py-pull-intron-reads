import sys
from readAlignmentFile import PyPullReadsAlignmentFileReader as AlignFileReader
from readGTFfile import PyPullReadsGTFfileReader as GTFreader
import argparse
import pysam
from readFilter import PyPullReadsFilter as ReadsFilter
from logger import PyPullReadsLogger as Logger


def new_main():
	parser = argparse.ArgumentParser(description="Py-Pull-Intron-Reads pulls mapped Illumina short reads from a BAM file that are not in a feature described by a GTF file. In addition, Py-Pull-Intron-Reads will pull reads with a 5' or 3' addition that maps to a noncontiguous location from the read's original location.")
	parser.add_argument('-a','--alignment',help='Alignment file',action='store',required=True)
	parser.add_argument('-t','--type',help="Type of alignment file, 'sam', 'bam', or 'cram'",action='store',default='bam',choices=['bam','sam','cram'])
	parser.add_argument('-g','--gtf',required=True,help='GTF file',action='store')
	parser.add_argument('--verbose',help='Turn on verbose logging',action='store_true',default=False)
	parser.add_argument('-l','--log',help='Log file',action='store',default=sys.stderr)
	parser.add_argument('-o','--output',help='Output file',action='store',default=sys.stdout)
	parser.add_argument('-s','--reportsplicing',help='Report exon-intron splicing sites as significant reads',action='store_true',default=False)
	parser.add_argument('-m','--reportmates',help='Report mates of stored reads',action='store_true',default=False)
	parser.add_argument('--minmapq',help='Minimum mapping quality to keep read',action='store',default=10)

	args = parser.parse_args()
	
	log_fh = None
	if args.log == sys.stderr:
		log_fh = Logger(args.log)
	else:
		try:
			log_fh = Logger(open(args.log, 'w'))
		except IOError as err:
			raise IOError('Unable to write log.')
	log_fh.log('Creating alignment file with filename %s' % args.alignment)	
	alignment_file = AlignFileReader(args.alignment,args.type,verbose=args.verbose,logger=log_fh)
	log_fh.log('Creating GTF interval tree from file %s' % args.gtf)
	gtf_db = GTFreader(args.gtf,verbose=args.verbose,logger=log_fh)


	readfilter = ReadsFilter(alignment_file,gtf_db,verbose=args.verbose,logger=log_fh)
	filtered_reads = readfilter.generateFilteredReads(report_exon_intron_splicing=args.reportsplicing, exon_criteria=('exon', 'transcript'),min_mapq=args.minmapq)
	
	output_file = pysam.AlignmentFile(args.output,'wb', template=alignment_file.alignmentfile)
	log_fh.log('Writing to output file %s' % args.output)
	if args.reportmates:
		for read in filtered_reads:
			output_file.write(read)
			mate = alignment_file.getReadMate(read)
			if mate:
				output_file.write(mate)
	else:
		for read in filtered_reads:
			output_file.write(read)

	output_file.close()
	log_fh.log('Done writing reads')
	log_fh.log('Total Reads: %s' % str(readfilter.total_reads))
	log_fh.log('Total Reads Kept: %s' % str(readfilter.spliced_reads + readfilter.intron_reads))
	log_fh.log('Spliced Reads Kept: %s' % str(readfilter.spliced_reads))
	log_fh.log('Intronic Reads Kept: %s' % str(readfilter.intron_reads))

	


if __name__ == '__main__':
	new_main()

