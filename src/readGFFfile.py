import gffutils

###
# Class PyPullReadsGFFfileReader:
#	Class to read GFF3/GTF File into sqlite3 database and methods to get features in regions
###

class PyPullReadsGFFfileReader:
	db = None
	logger = None
	verbose = False	

	###
	# Constructor
	#	Args:
	#		filename: String of GFF3/GTF filename
	#		dbfilename: String of sqlite3 database of GFF3/GTF filename
	#		database: Already created databsse, if it exists
	#	Returns:
	#		Object reference
	###
	def __init__(self, filename, dbfilename, verbose=False, logger=None,database=None):
		if database:
			self.db = gffutils.FeatureDB(database)
		else:
			self.db = gffutils.create_db(filename, dbfilename, force=True, merge_strategy='merge', id_spec=['ID','name'])
			self.db = gffutils.FeatureDB(dbfilename)
		self.verbose = verbose
		self.logger = logger
		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Created GTF/GFF3 Database')
	###
	# generateFeaturesWithinRange
	#	Args:
	#		chr: string of chromosome in GFF3/GTF record
	#		start: start coordinate
	#		end: end coordinate
	#		fully_contained: Boolean keyword arg to determine wther records returned are completely contained within the provided interval, default False
	#	Returns:
	#		Generator yielding features
	###
	def generateFeaturesWithinRange(self, chrom, rec_start, rec_end, fully_contained=False):
		for feature in self.db.region(region=(chrom,rec_start,rec_end),completely_within=fully_contained):
			#print(	'Read feature of type %s on %s from %d to %d' % (feature.featuretype, feature.seqid, feature.start, feature.end))

			if self.verbose:
				self.logger.log('Read feature of type %s on %s from %d to %d' % (feature.featuretype, feature.seqid, feature.start, feature.end))
			yield feature
