from readAlignmentFile import PyPullReadsAlignmentFileReader as AlignFileReader

class PyPullRepeatFilter:
	alignment = None
	gtf_db = None
	repeat_db = None
	verbose = False
	logger = None
	passing_reads = 0
	total_reads = 0
	###
	# Constructor
	#	Args:
	#		alignment : AlignFileReader for alignment
	#		gtf_db : GTF database
	#		repeat_db : RepeatMasker file database
	#		logger : logger object
	#	Returns:
	#		Object reference
	###
	def __init__(self,alignment,gtf_db,repeat_db,verbose=False, logger=None):
		self.verbose = verbose
		self.logger = logger
		self.alignment = alignment 
		self.gtf_db = gtf_db
		self.repeat_db = repeat_db
		
		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Created filtering object')

	###
	# filterReads
	#	Args:
	#		gene_criteria : list of strings indicating exons or other features indicating a gene (e.g. 'transcript', 'CDS', etc.); defaults to ['gene']
	#		min_distance : minimum distance from a gene to be considered a significant result; defualts to 1000
	#	Returns:
	#		List of reads mapping to intronic positions or reads with noncontiguous intron elements
	###
	def filterReads(self, gene_criteria=('gene'),min_distance=1000):
		passing_reads_list = list()
		for read in self.alignment.generateReads():
			self.total_reads += 1
			if self.filterRead(read,gene_criteria=gene_criteria,min_distance=min_distance):
				passing_reads_list.append(read)
				self.passing_reads += 1
		return passing_reads_list

	def generateFilteredReads(self, gene_criteria=('gene'),min_distance=1000):
		for read in self.alignment.generateReads():
			self.total_reads += 1
			if self.filterRead(read,gene_criteria=gene_criteria,min_distance=min_distance):
				self.passing_reads += 1
				yield read


	###
	# filterRead
	#	Args:
	#		read : AlignmentSegment to check
	#		gene_criteria : list of strings indicating genes or other features indicating a gene (e.g. 'transcript', 'CDS', etc.); defaults to ['gene']
	#	Returns:
	#		Boolean indicating whether the red passed the filter
	###
	def filterRead(self, read, gene_criteria=('gene'), min_distance=1000):
		filter_passed = False

		for ref_start, ref_end in rangesInList(read.get_reference_positions(full_length=False)):
			try:
				if any(map(lambda feature: feature.data.feature in gene_criteria, self.gtf_db.generateFeaturesEnclosingRange(self.alignment.alignmentfile.get_reference_name(read.reference_id),ref_start,ref_end))):
					#feature is within gene, so it is spliced
					filter_passed = True
					if self.verbose:
						self.logger.log('Read %s interval %s-%s is within gene.' % (str(read),str(ref_start),str(ref_end)))
				elif not any(map(lambda feature: feature.data.feature in gene_criteria, self.gtf_db.generateFeaturesWithinRange(self.alignment.alignmentfile.get_reference_name(read.reference_id),ref_start-min_distance,ref_end+min_distance))):
					#feature is not within gene and is not within min_distance of the gene
					if not any(self.repeat_db.generateFeaturesWithinRange(self.alignment.alignmentfile.get_reference_name(read.reference_id),ref_start,ref_end)):
						# read is not in repeat region
						filter_passed = True
						if self.verbose:
							self.logger.log('Read %s interval %s-%s is not within minimum distance of a gene and is not within a known repeat site.' % (str(read),str(ref_start),str(ref_end)))
			except ValueError as err:
				if self.verbose:
					self.logger.log(err)

		if self.verbose:
			if not filter_passed:
				self.logger.log('Read %s did not pass filter' % str(read))	
		
		return filter_passed

	
###
# rangesInList
#	Args:
#		data : list of numbers
#	Returns:
#		list of ranges assiciated with continuous runs in the data
###
def rangesInList(data):
	ranges = list()
	data.append(data[-1])
	diffs = [0 for x in range(0,len(data))]
	for x in range(1,len(data)):
		diffs[x] = data[x] - data[x-1]
	diffs[0] = diffs[1]
	#print diffs
	prev = 0
	start = 0
	for x in range(1,len(diffs)):
		if abs(diffs[x]) == 1 and (diffs[x]>0) == (diffs[x-1]>0):
			pass
		else:
			ranges.append((data[start],data[prev]))
			start = x
		prev = x
	return ranges
