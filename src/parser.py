import re
import math
from attrdict import attrdict
from intervaltree import Interval, IntervalTree

class GTFparser(object):
	#Reads each line of a gtf and converts it to a GTFrecord, then adds the record to a list
	def __init__(self, filename, verbose=False, logger=None):
		self.verbose = verbose
		self.logger = logger
		self.recordsByChromosome = dict()
		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Created GTF Database')

		with open(filename, 'r') as f:
			for line in f:
				record = self.parse_line(line)
				if record == None:
					continue
				if not record.seqname in self.recordsByChromosome:
					self.recordsByChromosome[record.seqname] = list()
					self.recordsByChromosome[record.seqname].append(record)
					if verbose:
						self.logger.log("Created new chromosome entry " + str(record.seqname))
						self.logger.log("Added to existing entry " + str(record))
				else:
					self.recordsByChromosome[record.seqname].append(record)
					if verbose:
						self.logger.log("Added to existing entry " + str(record))
				

				#for chrom, recs in self.recordsByChromosome.items():
				#	for rec in recs:
				#		self.logger.log(rec.items())
						
		
	
	
	#Parses a line of the gtf file
	def parse_line(self, line):
		line = line.strip()
		if re.match(r'^#',line):
			return None
		fields = line.split('\t')
		attributes = dict()
		for av_pair in filter(lambda x:len(x)>0,fields[8].strip().split(';')):
			(attr,val) = av_pair.strip().split(' ')
			attributes[attr]=val
		fields[8]=attributes

		while True:
			try:
				fields[fields.index('.')] = None
			except ValueError:
				break

		fields_dict = attrdict({ 'seqname' : fields[0],
		'source' : fields[1],
		'feature' : fields[2],
		'start' : None if fields[3] == None else int(fields[3]),
		'end' : None if fields[4] == None else int(fields[4]),
		'score' : None if fields[5] == None else int(fields[5]),
		'strand' : fields[6],
		'frame' : None if fields[7] == None else int(fields[7]),
		'attribute' : fields[8], })
		
		return fields_dict


	def generateIntervalTreeDict(self):
		treedict = dict()
		for chrom, records in self.recordsByChromosome.items():
			tree = None
			#records are noninclusive on the end, so we have to add one to the GTF-specified end
			if self.verbose:
				tree = IntervalTree()
				for record in records:
					self.logger.log('Adding record %s to tree' % str(record.items()))
					tree.add(Interval(record.start,record.end+1,record))
			else:
				tree = IntervalTree(
					[Interval(record.start,record.end+1,record) for record in records]
					)
			treedict[chrom] = tree
		return treedict

class RepeatParser(object):
	#Reads each line of a gtf and converts it to a GTFrecord, then adds the record to a list
	def __init__(self, filename, verbose=False, logger=None):
		self.verbose = verbose
		self.logger = logger
		self.recordsByChromosome = dict()
		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Created GTF Database')

		with open(filename, 'r') as f:
			for line in f:
				record = self.parse_line(line)
				if record == None:
					continue
				if not record.seqname in self.recordsByChromosome:
					self.recordsByChromosome[record.seqname] = list()
					self.recordsByChromosome[record.seqname].append(record)
					if verbose:
						self.logger.log("Created new chromosome entry " + str(record.seqname))
						self.logger.log("Added to existing entry " + str(record))
				else:
					self.recordsByChromosome[record.seqname].append(record)
					if verbose:
						self.logger.log("Added to existing entry " + str(record))
				

				#for chrom, recs in self.recordsByChromosome.items():
				#	for rec in recs:
				#		self.logger.log(rec.items())
						
		
	
	
	#Parses a line of the gtf file
	def parse_line(self, line):
		line = line.strip()
		if re.match(r'^\s*[A-Za-z]+',line):
			#print 'None returned.'
			return None
		fields = line.split()
		if not len(fields) == 15:
			return None
		while True:
			try:
				fields[fields.index('.')] = None
			except ValueError:
				break
		fields = map(lambda l:l.strip(), fields)
		fields_dict = attrdict({ 'seqname' : fields[4],
		'source' : 'RepeatMasker',
		'feature' : fields[10],
		'start' : None if fields[5] == None else int(fields[5]),
		'end' : None if fields[6] == None else int(fields[6]),
		'score' : None if fields[0] == None else int(fields[0]),
		'strand' : fields[8],
		'frame' : None if fields[14] == None else int(fields[14]),
		'attribute' : fields[9], })

		return fields_dict


	def generateIntervalTreeDict(self):
		treedict = dict()
		for chrom, records in self.recordsByChromosome.items():
			tree = None
			#records are noninclusive on the end, so we have to add one to the GTF-specified end
			if self.verbose:
				tree = IntervalTree()
				for record in records:
					self.logger.log('Adding record %s to tree' % str(record.items()))
					tree.add(Interval(record.start,record.end+1,record))
			else:
				tree = IntervalTree(
					[Interval(record.start,record.end+1,record) for record in records]
					)
			treedict[chrom] = tree
		return treedict

	
