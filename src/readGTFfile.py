from parser import GTFparser

###
# Class PyPullReadsGFFfileReader:
#	Class to read GFF3/GTF File into sqlite3 database and methods to get features in regions
###

class PyPullReadsGTFfileReader:
	itreedict = None
	logger = None
	verbose = False	

	###
	# Constructor
	#	Args:
	#		filename: String of GFF3/GTF filename
	#		dbfilename: String of sqlite3 database of GFF3/GTF filename
	#		database: Already created databsse, if it exists
	#	Returns:
	#		Object reference
	###
	def __init__(self, filename, verbose=False, logger=None):
		parser = GTFparser(filename, verbose=verbose, logger=logger)
		self.itreedict = parser.generateIntervalTreeDict()
		self.verbose = verbose
		self.logger = logger
		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Created GTF/GFF3 Database')
	###
	# generateFeaturesWithinRange
	#	Args:
	#		chr: string of chromosome in GFF3/GTF record
	#		start: start coordinate
	#		end: end coordinate
	#		fully_contained: Boolean keyword arg to determine wther records returned are completely contained within the provided interval, default False
	#	Returns:
	#		Generator yielding features
	###
	def generateFeaturesWithinRange(self, chrom, rec_start, rec_end):
			try:
				for iv in self.itreedict[chrom][rec_start:rec_end]:
					if self.verbose:
						self.logger.log('Read feature of type %s on %s from %d to %d' % (iv.data.feature, chrom, iv.begin, iv.end))
					yield iv
			except KeyError as err:
				raise ValueError('%s not found in chromosomes composing interval tree' % chrom)

	def generateFeaturesEnclosingRange(self, chrom, rec_start, rec_end):
		try:
			for iv in self.itreedict[chrom][rec_start:rec_end]:
				if self.verbose:
					self.logger.log('Read feature of type %s on %s from %d to %d' % (iv.data.feature, chrom, iv.begin, iv.end))
				if rec_start >= iv.begin and rec_end <= iv.end:
					if self.verbose:
						self.logger.log('Feature of type %s on %s from %d to %d completely encloses interval %d - %d' % (iv.data.feature, chrom, iv.begin, iv.end, rec_start, rec_end))
					yield iv
		except KeyError as err:
			raise ValueError('%s not found in chromosomes composing interval tree' % chrom)
		
