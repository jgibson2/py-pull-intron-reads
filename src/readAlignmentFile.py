import pysam
from logger import PyPullReadsLogger as Logger

####
# Class PyPullReadsAlignmentFileReader
#	Reads alignment file and generates individual reads
####

class PyPullReadsAlignmentFileReader:
	#cdef AlignmentFile file #cython extension
	alignmentfile = None
	logger = None
	verbose = False
	###
	# Constructor
	#	Args:
	#		filename : String of SAM/CRAM/BAM filename
	#		type: String of file type, options are 'bam', 'sam', or 'cram'
	#	Returns: 
	#		Object reference
	###
	def __init__(self, filename, type, verbose=False,logger=None): 
		self.alignmentfile = pysam.AlignmentFile(filename, self.generatePysamFileOpenArg(type))
		self.verbose = verbose
		self.logger = logger
		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Opened file of type %s' % type)
	###
	# generatePysamFileOpenArg
	#	Args:
	#		type: String of file type, options are 'bam', 'sam', or 'cram'
	#	Returns:
	#		String of argument for pysam's AlignmentFile constuctor
	###
	def generatePysamFileOpenArg(self,type):
		if type == 'bam':
			return 'rb'
		elif type == 'sam':
			return 'r'
		elif type == 'cram':
			return 'rc'
		else:
			raise ValueError("File is not BAM, CRAM, or SAM!")
	###
	# generateReads
	#	Args:
	#		None
	#	Returns:
	#		Generator yielding reads from file
	###
	def generateReads(self):
		#cdef AlignmentSegment read
		for read in self.alignmentfile:
			if self.verbose:
				self.logger.log('Read %s yielded' % str(read))	
			yield read
	
	###
	# getReadMates
	#	Args:
	#		reads : list of AlignmentSegment reads
	#	Returns:
	#		Generator of (read, mate) tuples
	###
	def getReadMates(self, reads):
		#cdef AlignmentSegment read
		#cdef AlignmentSegment mate
		for read in reads:
			mate = None
			try:
				mate = self.alignmentfile.mate(read)
			except ValueError:
				if self.verbose:
					self.logger.log("Read %s unpaired." % str(read))
			yield (read,mate)
	###
	# getReadMatesList
	#	Args:
	#		reads : list of AlignmentSegment reads
	#	Returns:
	#		List of (read, mate) tuples
	###
	def getReadMatesList(self, reads):
		#cdef AlignmentSegment read
		#cdef AlignmentSegment mate
		read_mate_pairs = list()
		for read in reads:
			mate = None
			try:
				mate = self.alignmentfile.mate(read)
			except ValueError:
				if self.verbose:
					self.logger.log("Read %s unpaired." % str(read))
			read_mate_pairs.append((read,mate))
		return read_mate_pairs

	###
	# getReadMate
	#	Args:
	#		reads : AlignmentSegment read
	#	Returns:
	#		mate
	###
	def getReadMate(self, read):
		#cdef AlignmentSegment mate
		mate = None
		try:
			mate = self.alignmentfile.mate(read)
		except ValueError:
			if self.verbose:
				self.logger.log("Read %s unpaired." % str(read))
		return mate
