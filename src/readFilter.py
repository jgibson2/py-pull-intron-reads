from readAlignmentFile import PyPullReadsAlignmentFileReader as AlignFileReader
from readGTFfile import PyPullReadsGTFfileReader as GTFreader
#from itertools import groupby
#from operator import itemgetter

class PyPullReadsFilter:
	alignment = None
	gff_db = None
	verbose = False
	logger = None
	spliced_reads = 0
	intron_reads = 0
	total_reads = 0
	###
	# Constructor
	#	Args:
	#		filename : String of SAM/CRAM/BAM filename
	#		type: String of file type, options are 'bam', 'sam', or 'cram'
	#		filename: String of GFF3/GTF filename
	#		dbfilename: String of sqlite3 database of GFF3/GTF filename
	#	Returns:
	#		Object reference
	###
	def __init__(self,alignment,gff_db,verbose=False, logger=None):
		self.verbose = verbose
		self.logger = logger
		self.alignment = alignment 
		self.gff_db = gff_db

		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Created filtering object')

	###
	# filterReads
	#	Args:
	#		exon_criteria : list of strings indicating exons or other features indicating a transcript (e.g. 'UTR', 'CDS', etc.); defaults to ['exon']
	#	Returns:
	#		List of reads mapping to intronic positions or reads with noncontiguous intron elements
	###
	def filterReads(self, exon_criteria=('exon'),report_exon_intron_splicing=True,min_mapq=10):
		intron_mapped_reads = list()
		#cdef AlignmentSegment read
		read = None
		for read in self.alignment.generateReads():
			self.total_reads += 1
			if self.filterRead(read,exon_criteria=exon_criteria,report_exon_intron_splicing=report_exon_intron_splicing,min_mapq=10):
				intron_mapped_reads.append(read)
		return intron_mapped_reads

	def generateFilteredReads(self, exon_criteria=('exon'),report_exon_intron_splicing=True,min_mapq=10):
		for read in self.alignment.generateReads():
			self.total_reads += 1
			if self.filterRead(read,exon_criteria=exon_criteria,report_exon_intron_splicing=report_exon_intron_splicing, min_mapq=10):
				yield read


	###
	# filterRead
	#	Args:
	#		read : AlignmentSegment to check
	#		exon_criteria : list of strings indicating exons or other features indicating a transcript (e.g. 'UTR', 'CDS', etc.); defaults to ['exon']
	#	Returns:
	#		List of reads mapping to intronic positions or reads with noncontiguous intron elements
	###
	def filterRead(self, read, exon_criteria=('exon'), report_exon_intron_splicing=True, min_mapq=10):
		total_exon_mapped_count = 0
		last_region_in_exon = False
		filter_passed = False
		not_first_interval = False

		#STAR assigns uniquely mapped reads to 255 :( (This is not standards-compliant)
		#if read.mapping_quality < min_mapq or read.mapping_quality == 255:
		if read.mapping_quality < min_mapq:
			return False

		for ref_start, ref_end in rangesInList(read.get_reference_positions(full_length=False)):
			#check if any of the features in the region are exons
			try:
				exon_mapped_interval = any(map(lambda feature: feature.data.feature in exon_criteria, self.gff_db.generateFeaturesWithinRange(self.alignment.alignmentfile.get_reference_name(read.reference_id),ref_start,ref_end)))
				if exon_mapped_interval: #interval mapped to exon(s)
					total_exon_mapped_count += 1
					if not last_region_in_exon and report_exon_intron_splicing and not_first_interval: #if we were in a noncoding region before, we want to report splicing, AND we are not in the first interval, then this is a significant signal
						filter_passed = True
						self.spliced_reads += 1
						#if the last interval was in intron and the current interval is an exon, the read is significant
						if self.verbose:
							self.logger.log('Read %s interval %s-%s mapped to noncontiguous exonic positions after noncoding region' % (str(read),str(ref_start),str(ref_end)))
					last_region_in_exon = True
					if self.verbose:
						self.logger.log('Read %s interval %s-%s matched exon criteria.' % (str(read),str(ref_start),str(ref_end)))
				else: #exon count equals zero
					if last_region_in_exon and report_exon_intron_splicing: #if last_region_in_exon has changed to True, then we must not be in the first interval
						filter_passed = True
						self.spliced_reads += 1
						if self.verbose:
							self.logger.log('Read %s interval %s-%s mapped to noncontiguous noncoding region after exon' % (str(read),str(ref_start),str(ref_end)))
						#if the last region mapped to an exon and the next noncontiguous interval maps to an intron, the read is significant
					last_region_in_exon = False
			except ValueError as err:
				if self.verbose:
					self.logger.log(err)
			not_first_interval = True #it is no longer the first interval

		if total_exon_mapped_count == 0:
			if self.verbose:	
				self.logger.log('Read %s mapped completely to noncoding positions' % (str(read)))
			filter_passed = True
			self.intron_reads += 1
		return filter_passed

	
###
# rangesInList
#	Args:
#		data : list of numbers
#	Returns:
#		list of ranges assiciated with continuous runs in the data
###
def rangesInList(data):
	ranges = list()
	data.append(data[-1])
	diffs = [0 for x in range(0,len(data))]
	for x in range(1,len(data)):
		diffs[x] = data[x] - data[x-1]
	diffs[0] = diffs[1]
	#print diffs
	prev = 0
	start = 0
	for x in range(1,len(diffs)):
		if abs(diffs[x]) == 1 and (diffs[x]>0) == (diffs[x-1]>0):
			pass
		else:
			ranges.append((data[start],data[prev]))
			start = x
		prev = x
	return ranges
	"""
	ranges = list()
	for k, g in groupby(enumerate(data), lambda x:x[0]-x[1]):
	    group = list(map(itemgetter(1), g))
	    ranges.append((group[0], group[-1]))
	return ranges
	"""
