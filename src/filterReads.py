import sys
from readAlignmentFile import PyPullReadsAlignmentFileReader as AlignFileReader
from readGTFfile import PyPullReadsGTFfileReader as GTFreader
from readRepeatFile import PyPullReadsRepeatFileReader as RepeatReader
import argparse
import pysam
from repeatFilter import PyPullRepeatFilter as RepeatFilter
from logger import PyPullReadsLogger as Logger
from windower import PyPullReadsWindower as Windower 


def new_main():
	parser = argparse.ArgumentParser(description="filterReads provides additional filtering based on gene starts and ends and on known repetitive elements")
	parser.add_argument('-a','--alignment',help='Alignment file',action='store',required=True)
	parser.add_argument('-t','--type',help="Type of alignment file, 'sam', 'bam', or 'cram'",action='store',default='bam',choices=['bam','sam','cram'])
	parser.add_argument('-g','--gtf',required=True,help='GTF file',action='store')
	parser.add_argument('-m','--repeats',required=True,help='RepeatMasker file',action='store')
	parser.add_argument('--verbose',help='Turn on verbose logging',action='store_true',default=False)
	parser.add_argument('-l','--log',help='Log file',action='store',default=sys.stderr)
	parser.add_argument('-o','--output',help='Output file',action='store',default=sys.stdout)
	parser.add_argument('-b','--bam',help='Output BAM file',action='store',required=True)
	parser.add_argument('-w','--windowsize',help='Size of windows over which read frequencies are measured',action='store',default=5000)
	parser.add_argument('-f','--frequency',help='Minimum number of reads needing to be present in a window to be considered significant',action='store',default=100)
	parser.add_argument('-r','--skipratio',help='Skipping ratio (fraction of window size to skip each time)',action='store',default=0.2)

	args = parser.parse_args()
	
	log_fh = None
	if args.log == sys.stderr:
		log_fh = Logger(args.log)
	else:
		try:
			log_fh = Logger(open(args.log, 'w'))
		except IOError as err:
			raise IOError('Unable to write log.')
	alignment_file = AlignFileReader(args.alignment,args.type,verbose=args.verbose,logger=log_fh)
	log_fh.log('Creating GTF interval tree from file %s' % args.gtf)
	gtf_db = GTFreader(args.gtf,verbose=args.verbose,logger=log_fh)
	log_fh.log('Creating RepeatMasker interval tree from file %s' % args.repeats)
	repeat_db = RepeatReader(args.repeats,verbose=args.verbose,logger=log_fh)
	repeat_filter = RepeatFilter(alignment_file,gtf_db,repeat_db,verbose=args.verbose,logger=log_fh)
	filtered_reads = repeat_filter.generateFilteredReads(min_distance=args.frequency)
	output_bam_file = pysam.AlignmentFile(args.bam,'wb', template=alignment_file.alignmentfile)

	log_fh.log('Writing to output BAM file %s' % args.bam)
	for read in filtered_reads:
		output_bam_file.write(read)
	output_bam_file.close()

	pysam.index(args.bam)
	filtered_alignment_file = pysam.AlignmentFile(args.bam,'rb')
	windower = Windower(filtered_alignment_file, verbose=args.verbose, logger=log_fh)
	log_fh.log('Creating BED file with filename %s' % args.output)
	output_file = open(args.output, 'w')
	log_fh.log('Writing to output BED file')
	for window in windower.createWindows(window_length=int(args.windowsize), skip_ratio=float(args.skipratio), min_score=int(args.frequency)):
		output_file.write('\t'.join([str(x) for x in window])+'\n')
	output_file.close()
	
	log_fh.log('Done writing reads')
	log_fh.log('Total Reads: %s' % str(repeat_filter.total_reads))
	log_fh.log('Total Reads Kept: %s' % str(repeat_filter.passing_reads))


if __name__ == '__main__':
	new_main()

