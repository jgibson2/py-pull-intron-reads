import pysam
from logger import PyPullReadsLogger as Logger
from readAlignmentFile import PyPullReadsAlignmentFileReader as AlignFileReader

class PyPullReadsWindower:
	windows = None
	"""
	Window format: (chrom, start, end, name, score, strand)
	These are the first 6 fields of the BED format
	"""
	filtered_alignment = None
	

	#########################################
	# __init__: Creates windowing object
	# 	Parameters:
	# 		filtered_alignment: Pysam file representing the filtered alignment (generated using PyPullReads)
	# 		verbose: boolean, True/False, turns on verbose logging when True
	# 		logger : logger object. Required when verbose is True
	# 	Returns: Windowing object
	#########################################
	def __init__(self, filtered_alignment, verbose=False, logger=None):
		self.filtered_alignment = filtered_alignment # Pysam file
		self.verbose = verbose
		self.logger = logger
		self.windows = list()
		
		if self.verbose:
			if logger==None:
				raise ValueError("Logger object not provided")
			self.logger.log('Created filtering object')

	#######################################
	# createWindows : generates list of windows with a significant number of reads
	# 	Parameters: 
	# 		window_length: length of windows to consider
	# 		skip_ratio: fraction of window lenght to skip in each iteration
	# 		min_score: minimum number of reads needed to be considered asignificant window
	# 	Returns: a list of window tuples containing reference, start, end, string representation, strand, and count (in order)
	#######################################
	def createWindows(self,window_length=10000, skip_ratio=0.2, min_score=200):
		#TODO: maybe replace with another IntervalTree implementation?
		ref_lengths = zip(self.filtered_alignment.references,self.filtered_alignment.lengths)
		for ref, length in ref_lengths:
			pos = 0 # inital position
			pos_win_start = 0 # initial window start
			pos_win_end = window_length # initial window end
			neg_win_start = 0
			neg_win_end = window_length
			pos_in_window = False
			neg_in_window = False

			def countPosStrand(read):
				return not read.is_reverse
			def countNegStrand(read):
				return read.is_reverse

			while pos <= length:
				# check count of positive strand
				if self.filtered_alignment.count(reference=ref, start=pos, end=pos+window_length, read_callback=countPosStrand) >= min_score:
					pos_in_window = True
					pos_win_end = pos + window_length
				elif pos_in_window:
					#end of window
					self.windows.append((ref,pos_win_start,pos_win_end,'%s:%d-%d' % (ref, pos_win_start, pos_win_end),self.filtered_alignment.count(reference=ref,start=pos_win_start,end=pos_win_end,read_callback=countPosStrand),'+'))
					if self.verbose:
						self.logger.log('Added window on %s from %d to %d on + strand' % (ref, pos_win_start, pos_win_end))
					pos_win_start = pos # (not needed?) + round(self.window_length*self.skip_ratio)
					pos_in_window = False
				else:
					pos_win_start = pos

				# check count of negative strand
				if self.filtered_alignment.count(reference=ref, start=pos, end=pos+window_length, read_callback=countNegStrand) >= min_score:
					neg_in_window = True
					neg_win_end = pos + window_length
				elif neg_in_window:
					#end of window
					self.windows.append((ref,neg_win_start,neg_win_end,'%s:%d-%d' % (ref, neg_win_start, neg_win_end),self.filtered_alignment.count(reference=ref,start=neg_win_start,end=neg_win_end,read_callback=countNegStrand),'-'))
					if self.verbose:
						self.logger.log('Added window on %s from %d to %d on - strand' % (ref, neg_win_start, neg_win_end))
					neg_win_start = pos # (not needed?) + round(self.window_length*self.skip_ratio)
					neg_in_window = False
				else:
					neg_win_start = pos
				
				pos += int(round(window_length*skip_ratio)) # extend position
		return self.windows
