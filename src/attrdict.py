class attrdict(dict):
	
	#attrdict extends dict, overloading the __getattr__ operator
	
	def __getattr__(self, attr):
		return self[attr]
	
	def __setattr__(self,attr,val):
		self[attr] = val

	def source():
		def fget(self): return source if hasattr(self, '_source') else None
		def fset(self, value): self._source = value
		return locals()
	source = property(**source())
